package com.daria.geocoder;

import com.daria.geocoder.dto.*;
import com.daria.geocoder.exception.NominatimIntegrationException;
import com.daria.geocoder.service.GeocoderService;
import org.apache.coyote.BadRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GeocoderApplicationTests {

    @Mock
    RestTemplate restTemplate;

    private final static String LATITUDE_EXCEPTION_MESSAGE = "The latitude must be between -90 and 90";
    private final static String LONGITUDE_EXCEPTION_MESSAGE = "The longitude must be between -180 and 180";
    private final static String LANGUAGE_EXCEPTION_MESSAGE = "The language must be ISO 639-1";
    private final static String INTEGRATION_EXCEPTION_MESSAGE = "The problem with Nominatim integration occurred";
    private final static String ADDRESS_VALUE = "address_value";


    @Test
    public void testSearchAddress() throws BadRequestException {
        GeocoderService geocoderService = new GeocoderService(restTemplate);

        Exception smallLatitudeException = assertThrows(BadRequestException.class, () -> geocoderService.searchAddress(-91d, 90d, "ru"));
        assertEquals(LATITUDE_EXCEPTION_MESSAGE, smallLatitudeException.getMessage());

        Exception bigLatitudeException = assertThrows(BadRequestException.class, () -> geocoderService.searchAddress(91d, 90d, "ru"));
        assertEquals(LATITUDE_EXCEPTION_MESSAGE, bigLatitudeException.getMessage());

        Exception smallLongitudeException = assertThrows(BadRequestException.class, () -> geocoderService.searchAddress(90d, -181d, "ru"));
        assertEquals(LONGITUDE_EXCEPTION_MESSAGE, smallLongitudeException.getMessage());

        Exception bigLongitudeException = assertThrows(BadRequestException.class, () -> geocoderService.searchAddress(90d, 181d, "ru"));
        assertEquals(LONGITUDE_EXCEPTION_MESSAGE, bigLongitudeException.getMessage());

        Exception nonExistentLanguageException = assertThrows(BadRequestException.class, () -> geocoderService.searchAddress(90d, 180d, "non_existent_language"));
        assertEquals(LANGUAGE_EXCEPTION_MESSAGE, nonExistentLanguageException.getMessage());

        Exception nominatimIntegrationException = assertThrows(NominatimIntegrationException.class, () -> geocoderService.searchAddress(90d, 180d, "en"));
        assertEquals(INTEGRATION_EXCEPTION_MESSAGE, nominatimIntegrationException.getMessage());

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), isNull(), any(ParameterizedTypeReference.class))).thenReturn(ResponseEntity.ok(SearchAddressNominatimResponseDTO.builder().address(ADDRESS_VALUE).build()));
        SearchAddressResponseDTO res = geocoderService.searchAddress(90d, 180d, "en");
        assertEquals(ADDRESS_VALUE, res.getAddress());

    }

    @Test
    public void testSearchLocation() throws BadRequestException {
        GeocoderService geocoderService = new GeocoderService(restTemplate);

        Exception nonExistentLanguageException = assertThrows(BadRequestException.class, () -> geocoderService.searchLocation(ADDRESS_VALUE, "non_existent_language"));
        assertEquals(LANGUAGE_EXCEPTION_MESSAGE, nonExistentLanguageException.getMessage());

        Exception nominatimIntegrationException = assertThrows(NominatimIntegrationException.class, () -> geocoderService.searchLocation(ADDRESS_VALUE, "en"));
        assertEquals(INTEGRATION_EXCEPTION_MESSAGE, nominatimIntegrationException.getMessage());

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), isNull(), any(ParameterizedTypeReference.class))).thenReturn(ResponseEntity.ok(List.of(SearchLocationNominatimResponseDTO.builder().latitude(90d).longitude(90d).build())));
        SearchLocationResponseDTO res = geocoderService.searchLocation(ADDRESS_VALUE, "en");
        assertEquals(90d, res.getSearchLocationValueDTOList().get(0).getLatitude());
        assertEquals(90d, res.getSearchLocationValueDTOList().get(0).getLongitude());

    }
}