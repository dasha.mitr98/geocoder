package com.daria.geocoder.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchLocationResponseDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("locations")
    private List<SearchLocationValueDTO> searchLocationValueDTOList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String error;

}
