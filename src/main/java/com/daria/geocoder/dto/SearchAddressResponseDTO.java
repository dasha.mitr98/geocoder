package com.daria.geocoder.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchAddressResponseDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("value")
    private String address;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String error;

}
