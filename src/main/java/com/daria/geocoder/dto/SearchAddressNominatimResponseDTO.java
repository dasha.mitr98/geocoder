package com.daria.geocoder.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchAddressNominatimResponseDTO {
    @JsonProperty("display_name")
    private String address;

    public SearchAddressResponseDTO mapToSearchAddressResponseDTO() {
        return SearchAddressResponseDTO
                .builder()
                .address(this.address)
                .build();

    }
}
