package com.daria.geocoder.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchLocationNominatimResponseDTO {
    @JsonProperty("lat")
    private Double latitude;
    @JsonProperty("lon")
    private Double longitude;
    @JsonProperty("display_name")
    private String address;

    public SearchLocationValueDTO mapToSearchLocationValueDTO() {
        return SearchLocationValueDTO
                .builder()
                .latitude(this.latitude)
                .longitude(this.longitude)
                .address(this.address)
                .build();

    }


}
