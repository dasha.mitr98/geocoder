package com.daria.geocoder.service;

import com.daria.geocoder.dto.*;
import com.daria.geocoder.exception.NominatimIntegrationException;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.BadRequestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class GeocoderService {

    @Value("${geocoder.nominatim.url}")
    String nominatimUrl;
    @Value("${geocoder.nominatim.search_address_urn_template}")
    String nominatimSearchAddressUrnTemplate;
    @Value("${geocoder.nominatim.search_location_urn_template}")
    String nominatimSearchLocationUrnTemplate;
    private final RestTemplate restTemplate;

    @Cacheable("address")
    public SearchAddressResponseDTO searchAddress(Double latitude, Double longitude, String language) throws BadRequestException {
        validateLatitude(latitude);
        validateLongitude(longitude);
        validateLanguage(language);
        String url = String.format(nominatimUrl + nominatimSearchAddressUrnTemplate, latitude, longitude, language);
        try {
            return Objects.requireNonNull(restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<SearchAddressNominatimResponseDTO>() {
            }).getBody()).mapToSearchAddressResponseDTO();
        } catch (Exception e) {
            throw new NominatimIntegrationException("The problem with Nominatim integration occurred");
        }
    }

    @Cacheable("location")
    public SearchLocationResponseDTO searchLocation(String address, String language) throws BadRequestException {
        validateLanguage(language);
        String url = String.format(nominatimUrl + nominatimSearchLocationUrnTemplate, address, language);
        try {
            List<SearchLocationValueDTO> searchLocationValueDTOList = Objects.requireNonNull(restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<SearchLocationNominatimResponseDTO>>() {
            }).getBody()).stream().map(SearchLocationNominatimResponseDTO::mapToSearchLocationValueDTO).toList();
            return SearchLocationResponseDTO.builder().searchLocationValueDTOList(searchLocationValueDTOList).build();
        } catch (Exception e) {
            throw new NominatimIntegrationException("The problem with Nominatim integration occurred");
        }
    }

    private void validateLatitude(Double latitude) throws BadRequestException {
        if (latitude > 90 || latitude < -90)
            throw new BadRequestException("The latitude must be between -90 and 90");
    }

    private void validateLongitude(Double longitude) throws BadRequestException {
        if (longitude > 180 || longitude < -180)
            throw new BadRequestException("The longitude must be between -180 and 180");
    }

    private void validateLanguage(String language) throws BadRequestException {
        if (!Set.of(Locale.getISOLanguages()).contains(language))
            throw new BadRequestException("The language must be ISO 639-1");
    }

}
