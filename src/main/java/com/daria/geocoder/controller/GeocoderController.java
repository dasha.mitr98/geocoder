package com.daria.geocoder.controller;

import com.daria.geocoder.dto.SearchAddressResponseDTO;
import com.daria.geocoder.dto.SearchLocationResponseDTO;
import com.daria.geocoder.exception.NominatimIntegrationException;
import com.daria.geocoder.service.GeocoderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.BadRequestException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@Slf4j
@RestController
@RequestMapping("geocoder/search")
@RequiredArgsConstructor
public class GeocoderController {


    private final GeocoderService geocoderService;

    @GetMapping("/address")
    public ResponseEntity<SearchAddressResponseDTO> searchAddress(@RequestParam Double lat, @RequestParam Double lng, @RequestParam String lang) {
        try {
            return ResponseEntity.ok().body(geocoderService.searchAddress(lat, lng, lang));
        } catch (BadRequestException e) {
            return ResponseEntity.status(BAD_REQUEST).body(SearchAddressResponseDTO.builder().error(e.getMessage()).build());
        } catch (NominatimIntegrationException e) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(SearchAddressResponseDTO.builder().error(e.getMessage()).build());
        }

    }


    @GetMapping("/location")
    public ResponseEntity<SearchLocationResponseDTO> searchAddress(@RequestParam String q, @RequestParam String lang) {
        try {
            return ResponseEntity.ok().body(geocoderService.searchLocation(q, lang));
        } catch (BadRequestException e) {
            return ResponseEntity.status(BAD_REQUEST).body(SearchLocationResponseDTO.builder().error(e.getMessage()).build());
        } catch (NominatimIntegrationException e) {
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(SearchLocationResponseDTO.builder().error(e.getMessage()).build());
        }
    }


}
