package com.daria.geocoder.exception;

public class NominatimIntegrationException extends RuntimeException {
    public NominatimIntegrationException(String message) {
        super(message);
    }
}
